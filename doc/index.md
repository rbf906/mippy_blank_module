# ACR MRI Distortion Measurement

## How to use

### 1. Select image slice
The ACR distortion measurement uses the "grid" slice of the ACR phantom.  If this
slice is not selected by default, please scroll through to the correct slice.

For this analysis, the air-bubble in the phantom must be at the top of the image,
as shown below.  If the image is not the correct way up, please rotate the image,

*Note - if the image disappears when you rotate, just reset the window/level by
double clicking on the image with the RIGHT mouse button.*

![Orientation example](img/orientation_example.jpg "Correct image orientation for distortion assessment")

### 2. Locate the centre of the phantom
Press **Find central grid point**, and MIPPY will estimate where the centre of the phantom
is.  The yellow circle should contain the central crossing grid point, as shown below.  If
the circle is not in the correct location, click and drag to move it to the correct place.

![Centre point](img/center_point.jpg "Yellow circle should contain the central grid point")

### 3. Run distortion analysis
Press **Measure distortion** to perform the measurement. The program will use the central grid
point to draw 7 radial profiles through the phantom, and will fit each of the gridpoints. The
result should look like the image below.

![Distortion measurement](img/profiles.jpg "Results of the distortion analysis")

### 4. Save results to the database
Check the phantom and coil selections detected by MIPPY, and correct as necessary. When you
are happy that your results are correct, press **Save results to DB** to save your results. If
you are not happy with the results, please re-do the analysis.

## How it works

The analysis currently uses the *radial profiles* and not the grid points when calculating
linearity and distortion.  It measures the size of the phantom by detecting the phantom edges at
either end of each profile, and returns 7 measurements of the phantom diameter.

Linearity = Mean of the phantom diameter measurements</br>
Distortion = Coefficient of variation of the phantom diameter measurements

For the ACR phantom, perfect radial linearity = **190.0 mm**

Perfect distortion = **0.00 %**
